include ../container/packages.mk

# Required by many shellscripts
FEDORA_ROOT_PACKAGES += which

# Required by javascript build jobs
FEDORA_ROOT_PACKAGES += nodejs
FEDORA_ROOT_PACKAGES += yarnpkg
FEDORA_ROOT_PACKAGES += npm

# Required for python installation
FEDORA_ROOT_PACKAGES += python3-pip
FEDORA_ROOT_PACKAGES += python3-ujson
FEDORA_ROOT_PACKAGES += python3-setuptools
# Required by clientconfig
FEDORA_ROOT_PACKAGES += rsync
# Required by clientconfig
FEDORA_ROOT_PACKAGES += xz
# Required by distribution
FEDORA_ROOT_PACKAGES += zstd
# Required for python wheel/compiling
FEDORA_ROOT_PACKAGES += gcc
# Required for python wheel/compiling
FEDORA_ROOT_PACKAGES += python3-devel

# Required by ModioAB/mytemp-backend tests
FEDORA_ROOT_PACKAGES += python3-dbus

# Commonly used linter
FEDORA_ROOT_PACKAGES += ShellCheck

# Used by ModioAB/modio-contain
FEDORA_ROOT_PACKAGES += squashfs-tools
