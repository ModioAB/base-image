FEDORA_ROOT_PACKAGES += fedora-release
FEDORA_ROOT_PACKAGES += dnf

# Fedora 40 uses DNF 4 by default, but has DNF 5 as an option from the future
ifeq ($(FEDORA_ROOT_RELEASE),40)
FEDORA_ROOT_PACKAGES += dnf5
endif

# Container runtime
FEDORA_ROOT_PACKAGES += buildah
FEDORA_ROOT_PACKAGES += crun
FEDORA_ROOT_PACKAGES += fuse-overlayfs
FEDORA_ROOT_PACKAGES += podman
FEDORA_ROOT_PACKAGES += runc

# Basic build tools
FEDORA_ROOT_PACKAGES += curl
FEDORA_ROOT_PACKAGES += findutils
FEDORA_ROOT_PACKAGES += git
FEDORA_ROOT_PACKAGES += make
FEDORA_ROOT_PACKAGES += tar
